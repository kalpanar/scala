import org.apache.spark.SparkContext
import org.apache.spark.SparkConf

object Youtube_data_analysis {
  
  def data_analysis(){
    
     //Create conf object
      val conf = new SparkConf().setMaster("local[2]")
                                .setAppName("Youtube_data_analysis")

      //create spark context object
      val sc = new SparkContext(conf)
      val a = sc.textFile("C:/Users/smvj0/Downloads/youtubedata.txt")

      // Top 5 categories with max video uploaded
       var category = a.map(line=>line.split("\t")).filter(columns=>columns.length>=3).map(columns=>columns(3))
       var count = category.map(x=>(x,1))
       val res = count.reduceByKey(_+_).map(x=>(x.swap)).sortByKey(false).take(5).foreach(println)

       
        //Top 10 rated videos
        var rating = a.filter{x=>{if(x.toString().split("\t").length>=6)true else false}}.map(line=>{line.toString().split("\t")})
        val pairs = rating.map(x=>{(x(0),x(6).toDouble)})
        val toprated = pairs.reduceByKey(_+_).map(x=>(x.swap)).sortByKey(false).take(10).foreach(println)

        
        //Most viewed videos
        val viewed = rating.map(x=>{(x(0),x(5).toInt)})
        val video_views = viewed.reduceByKey(_+_).map(x=>(x.swap)).sortByKey(false).take(1).foreach(println)
       
}
 
  def main(args:Array[String]){
   data_analysis()
  }
}